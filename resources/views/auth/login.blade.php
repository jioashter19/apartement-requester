@extends('layouts.app')

@section('content')
<div class="bg-gradient-primary">
<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-5 col-lg-8 col-md-12">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                <!-- <div class="h4 text-gray-900 mb-4">{{ __('Login Page') }}</div> -->
                <div class="h4 text-gray-900 mb-4"><strong>LOGIN</strong> <br> <h6>Apartement Maintenance</h6></div>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-6 col-form-label text-md-left"></label><!-- {{ __('Email or Username') }} -->

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email or Username" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-6 col-form-label text-md-left"><!-- {{ __('Password') }} --></label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>


                        <!-- Divider -->
                        <hr class="sidebar-divider my-0"><br>
                        <div class="form-group row mb-0">
                            <div class="col-md-9 offset-md-2">
                                <button type="reset" class="btn btn-danger">Cancel</button>
                                <button type="submit" class="btn btn-primary">Login</button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                                <!-- Divider -->
                                <hr class="sidebar-divider my-0">
                                @if (Route::has('register'))
                                    <a class="btn-link" href="{{ route('register') }}">{{ __('Register New Account') }}</a>
                            @endif
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Divider -->
                <!-- <hr class="sidebar-divider my-0"> -->
                <!-- Footer -->
                  <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                      <div class="copyright text-center my-auto">
                        <span>Copyright &copy;2019 Junior.</span>
                      </div>
                    </div>
                  </footer>
                  <!-- End of Footer -->
            </div>
        </div>
    </div>
</div>
@endsection
