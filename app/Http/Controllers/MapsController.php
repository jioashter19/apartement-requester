<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MapsController extends Controller
{
	//Controller
    $markers = $qualityEvents->map(function ($event, $key) {

            return [
                    'latlng' => [$event->Latitude, $event->Longitude],
                    'time' => Carbon::parse($event->EventDateTime)->format('H:i'),
                    'date' => Carbon::parse($event->EventDateTime)->format('d-m-Y'),
                    'driver' => trim($event->DriverID ?? '--'),
                    'location' => $event->Location ?? '--',
                    'site' => $event->SiteID,
                    'speed' => intval(($event->CANSpeedInKilometresPerHour ?? $event->GPSSpeedInKilometresPerHour) * 0.621371192) . 'mph',
                    'course' => $event->GPSCourseInDegrees,
                ];
        })->values();

    //Javascript view map
    	var markers = {!! json_encode($markers) !!}

    //markers along the tracking line
        markers.map( function(item) {
            tip =     '<h4>Time: ' + item.time + '<small> on '+ item.date +'</small><br />'
                    + '<small>Location: ' + item.location + '</small><br />'
                    + '<small>Driver: ' + item.driver + '</small><br />'
                    + '<small>Speed: ' + item.speed; + '</small></h4>'

            if(item.site == ''){
                marker = L.marker(item.latlng,{rotationAngle: item.course, opacity:0})
                    .bindPopup(tip);
                    allmarkers.addLayer(marker);

            } else {
                L.circleMarker(item.latlng,{radius:7, color:'#0ff',stroke:false, fill:true, fillOpacity:1})
                    .addTo(mymap)
                    .bindPopup(tip);
            }
        })
}
