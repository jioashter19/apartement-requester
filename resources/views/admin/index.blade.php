@extends('admin.layouts_admin')

@section('content')
     <!-- Begin Page Content -->
        <div class="container-fluid">
        
     <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h4 class="m-0 font-weight-bold text-primary">User List</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Role</th>
                      <th>Created_at</th>
                      <th>Updated_at</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php
                        $a= 1;
                    @endphp

                     @foreach($data as $d)
                    <tr>
                      <td>{{ $a}}</td>
                      <td>{{ $d->name }}</td>
                      <td>{{ $d->email }}</td>
                      <td>{{ $d->role }}</td>
                      <td>{{ $d->created_at }}</td>
                      <td>{{ $d->updated_at }}</td>
                      <td>
                      <div class="btn-group" role="group">
                      <a href="/ad/{{$d->id}}/edit" class="btn btn-info btn-circle btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i>

                      <a href="/admdel/{{$d->id}}/delete" class="btn btn-danger btn-circle btn-sm" value="delete" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i>
                      </a>
                      </div>   
                      </td>
                    </tr>
                      @php
                          $a++;
                      @endphp
                      @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          </div>
        <!-- /.container-fluid -->
@endsection