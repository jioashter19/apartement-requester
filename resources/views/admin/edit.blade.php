@extends('admin.layouts_admin')
    
@section('content')
<div class="container-fluid">
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h4 class="m-0 font-weight-bold text-primary">User List</h4>
        </div>
            <div class="card-body">
              <div class="table-responsive">
              <form  action="/adm/{{$admin->id}}/update" method="POST">
                      {{csrf_field()}}
                    <div class="form-group">
                        Nama
                        <input type="text" class="form-control" name="name" value="{{$admin->name}}">
                    </div>
                    <div class="form-group">
                        Email Address or Username
                        <input type="text" class="form-control" name="email" value="{{$admin->email}}">
                    </div>
                    <!-- <div class="form-group">
                        Password
                        <input type="text" class="form-control" name="email" value="{{$admin->password}}">
                    </div> -->
                    <button type="submit" class="btn btn-success"><i class="fa fa-retweet"></i> Update</button>

                    <button type="cancel" class="btn btn-danger"><i class="fa fa-backspace"></i> Cancel</button>

                    </form>
                    </div>
                </div>
           </div>
        </div>
    </div>
@endsection
