<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\users;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = users::all();
        return view('admin.index', compact('data'));
    }

    public function edit($id)
    {
        $admin = \App\users::find($id);
        return view('admin/edit',['admin'=> $admin]);
    }

    public function update(Request $request, $id)
    {
        $datauser = \App\users::find($id);
        $datauser->update($request->all());
        return redirect('/admin')->with('Sukses','Data Berhasil di Update');
    }

    public function delete($id)
    {
        $datauser = \App\users::find($id);
        $datauser->delete($datauser);
        return redirect('/admin')->with('Sukses','Data Berhasil di Delete');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
        
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
        
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
