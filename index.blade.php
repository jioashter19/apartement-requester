@extends('maps.layouts_maps')

@section('maps')

	<script>
	var map = L.map('map').setView([-6.160700,106.775800], 12);
	    mapLink =
	        '<a href="http://openstreetmap.org">OpenStreetMap</a>';
	    L.tileLayer(
	        'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	         attribution: '&copy; ' + mapLink + ' Contributors',
	         maxZoom: 18,
	    }).addTo(map);
	</script>
@endsection