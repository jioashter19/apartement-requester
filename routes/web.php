<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//Route User
Route::get('/users', function () {
	return view('users.index');
});

//Route maps
Route::get('/maps', function () {
    return view('maps.index');
});


//Route Admin
Route::group(['middleware' => ['admin']], function(){
	Route::get('/admin', 'HomeController@admin')->name('admin');
	Route::get('/home', 'HomeController@users')->name('users');
});

Route::get('/admin','AdminController@index');
Route::get('/ad/{id}/edit','AdminController@edit');
Route::post('/adm/{id}/update','AdminController@update');
Route::get('/admdel/{id}/delete','AdminController@delete');

// Route::get('/admin/userstable', function () {
// 	return view('userstable.index');
// });

//Route Users
Route::get('/users','crud@index');
Route::get('/us/{id}/edit','crud@edit');
Route::post('/use/{id}/update','crud@update');
Route::get('/del/{id}/delete','crud@delete');




