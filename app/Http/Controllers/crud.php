<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\users;

class crud extends Controller
{
    public function index()
    {
    	$data = users::all();
    	return view('users/index', compact('data'));
    }

    public function edit($id)
    {
        $users = \App\users::find($id);
        return view('users/edit',['users'=> $users]);
    }

    public function update(Request $request, $id)
    {
        $ruangan = \App\users::find($id);
        $ruangan->update($request->all());
        return redirect('/users')->with('Sukses','Data Berhasil di Update');
    }

    public function delete($id)
    {
        $ruangan = \App\users::find($id);
        $ruangan->delete($ruangan);
        return redirect('/users')->with('Sukses','Data Berhasil di Delete');
    }

    
}
